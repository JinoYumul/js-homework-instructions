/*
	1. Write a function that takes two numbers as its parameters and prints in the console whichever number is higher.

	Stretch goal: Add a condition that prints in the console that the numbers are the same, if they are the same.


	2. Write a function that takes one of the following languages as its parameter:

	a. French
	b. Spanish
	c. Japanese

	Depending on which language is given, the function prints "Hello World" in that language, in the console.

	Stretch goal: Add a condition that asks the user to input a valid language if the given parameter does not match any of the 3 languages.

	3. Write a function that takes a number between 0 and 100 as its parameter. If the number is equal to or greater than 90, the function prints in the console the following message:

	"Your grade is A"

	For 80 to 89:

	"Your grade is B"

	For 70 to 79:

	"Your grade is C"

	For 60 to 69:

	"Your grade is D"

	For any number below 60:

	"Your grade is F"

	Stretch goal: Write a condition that prints a message saying the parameter must be a number between 0 and 100 if the given number is not between 0 and 100.

	Stress goal: Accomplish all of the above using only two return or console.log statements.
*/